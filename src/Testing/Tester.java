package Testing;
import java.awt.EventQueue;
import java.sql.SQLException;

import dataBase.Start;


public class Tester {

	private static Start frame;

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// UIManager.put("ScrollBarUI", "MYGUI.MetroScrollBar");
					frame = new Start();
					frame.setResizable(false);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
