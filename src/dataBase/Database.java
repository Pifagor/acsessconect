package dataBase;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public enum Database {
	DB;
	private String base_name = "Dekanat.accdb";
	// private String base_user = "atm";
	// private String base_pass = "mndfra19";

	// private String dbName = "//" + base_server + "/" + base_name + "?user=" +
	// base_user + "&password=" + base_pass;
	private Connection connection;
	private Statement st;
	private ResultSet rs;
	private String db;

	Database() {
		init();
	}

	private void init() {
		String path = new java.io.File(base_name).getAbsolutePath();
		db = "JDBC:ODBC:Driver=Microsoft Access Driver (*.mdb, *.accdb); DBQ="
				+ path;

		try {
			doConnection();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void doConnection() throws ClassNotFoundException, SQLException {
		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		connection = DriverManager.getConnection(db);
		st = connection.createStatement();

	}

	public List<Object[]> getTableData(String table, String[] colums,
			String[] types) {
		List<Object[]> res = new ArrayList<Object[]>();

		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM `" + table + "` ");

			while (rs.next()) {
				Object[] temp = new Object[colums.length];

				for (int i = 0; i < colums.length - 1; ++i) {
					String val = rs.getString(colums[i]);
					String type = types[i];

					if (type.equals("INTEGER") || type.equals("COUNTER")) {
						if (val == null)
							temp[i] = 0;
						else
							temp[i] = (Integer) Integer.parseInt(val);
					} else if (type.equals("DOUBLE") || type.equals("FLOAT")) {
						if (val == null)
							temp[i] = 0;
						else
							temp[i] = (Double) Double.parseDouble(val);
					} else if (type.equals("BIT")) {
						// temp[i] = Boolean.parseBoolean(val);
						if (val.equals("0"))
							temp[i] = "false";
						else
							temp[i] = "true";
					} else if (type.equals("DATETIME")) {
						if (val == null)
							temp[i] = "NULL!!!";
						else
							temp[i] = java.sql.Timestamp.valueOf(val);
						
						// System.out.println(temp[i]);
					} else {
						temp[i] = val;
					}

				}

				temp[colums.length - 1] = false;
				res.add(temp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public List<String> getTables() {
		List<String> tables = new ArrayList<String>();

		DatabaseMetaData md;
		try {
			md = connection.getMetaData();

			ResultSet rs = md.getTables(null, null, "%", null);
			String temp;
			while (rs.next()) {
				temp = rs.getString(3);
				// skipes smt acsses qwery
				if ((!temp.contains("MSys")) && (temp.charAt(0) < 'z')) {
					System.out.println(temp);
					tables.add(temp);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tables;
	}

	public String[] getColTypes(String table, String[] cols) {
		ResultSet rs;
		String[] res = null;
		try {
			rs = st.executeQuery("SELECT * FROM `" + table+"`");

			ResultSetMetaData rsmd = rs.getMetaData();
			int NumOfCol = rsmd.getColumnCount();

			res = new String[NumOfCol + 1];
			for (int i = 0; i < NumOfCol; i++) {
				System.out.println("Name of [" + i + "] Column data type is ="
						+ rsmd.getColumnTypeName(i + 1));
				res[i] = rsmd.getColumnTypeName(i + 1);
			}
			res[NumOfCol] = "boolean";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return res;
	}

	private String getColType(String table, String string) {
		ResultSet rs;
		try {
			rs = st.executeQuery("SELECT * FROM " + table);

			ResultSetMetaData rsmd = rs.getMetaData();
			int NumOfCol = rsmd.getColumnCount();
			for (int i = 1; i <= NumOfCol; i++) {
				System.out.println("Name of [" + i + "] Column data type is ="
						+ rsmd.getColumnTypeName(i));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private String getClassColType(String table, Class<?> columnClass) {

		if (columnClass.equals(String.class)) {
			return "String";
		} else if (columnClass.equals(Integer.class)
				|| columnClass.equals(Short.class)
				|| columnClass.equals(Byte.class)) {
			return "Integer";
		} else if (Number.class.isAssignableFrom(columnClass)) {
			return "Numeric";
		} else if (Date.class.isAssignableFrom(columnClass)) {
			return "Date";
		} else if (columnClass.equals(Object.class)) {
			// todo: Quick and dirty hack, as the formula never knows what type
			// is returned.
			return "String";
		} else {

			System.out.println("Unknown class: " + columnClass.toString());

		}
		return null;
	}

	public List<String> getColums(String table) {
		List<String> colums = new ArrayList<String>();

		try {
			// Statement st = connection.createStatement();
			// ResultSet rs =
			// st.executeQuery("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE `TABLE_NAME`='"+table+"' ");
			ResultSet rs = st.executeQuery("SELECT * FROM `" + table+"`");
			ResultSetMetaData rsmd = rs.getMetaData();
			// String firstColumnName = rsmd.getColumnName(1);
			int columnCount = rsmd.getColumnCount();

			// The column count starts from 1
			for (int i = 1; i < columnCount + 1; i++) {
				String name = rsmd.getColumnName(i);
				System.out.println(name);
				colums.add(name);
				// Do stuff with name
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return colums;
	}

	public void updateRow(String table, Object[] row, String[] names,
			String[] types) {
		try {

			String query = "UPDATE `" + table + "` SET ";

			for (int i = 1; i < types.length - 1; ++i) {
				query += "`" + names[i] + "` = (?)";
				if (i != types.length - 2) {
					query += ",";
				}
			}

			query += " WHERE `���`=(?)";

			PreparedStatement ps = connection.prepareStatement(query);

			for (int i = 1; i < types.length - 1; ++i) {
				String type = types[i];
				Object obj = row[i];

				if (type.equals("INTEGER") || type.equals("COUNTER")) {
					ps.setInt(i, (int) obj);
				} else if (type.equals("DOUBLE") || type.equals("FLOAT")) {
					ps.setDouble(i, (double) obj);
				} else if (type.equals("BIT")) {
					ps.setBoolean(i, (Boolean) obj);
				} else if (type.equals("DATETIME")) {
					java.sql.Timestamp x = (java.sql.Timestamp) obj;
					ps.setTimestamp(i, x);
				} else {
					ps.setString(i, (String) obj);
				}

			}

			ps.setInt(types.length - 1, (Integer) row[0]);

			ps.execute();

		} catch (SQLException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"Something went wrong while UPDATING a table. Check data types or field length.");
			Start.error_flag = true;
			e.printStackTrace();
		}
	}

	public void insertRow(String table, Object[] row, String[] names,
			String[] types) {
		try {
			
			String query = "INSERT INTO `" + table + "`  (";

			for (int i = 1; i < types.length - 1; ++i) {
				query += "[" + names[i] + "]";
				if (i != types.length - 2) {
					query += ",";
				}
			}
			query += ") VALUES (";
			for (int i = 1; i < types.length - 1; ++i)
			{
				query += "(?)";
				if (i != types.length - 2) {
					query += ",";
				}
			}
			query += ")";
			
			PreparedStatement ps = connection.prepareStatement(query);

			for (int i = 1; i < types.length - 1; ++i) {
				String type = types[i];
				Object obj = row[i];

				if (type.equals("INTEGER") || type.equals("COUNTER")) {
					ps.setInt(i, (int) obj);
				} else if (type.equals("DOUBLE") || type.equals("FLOAT")) {
					ps.setDouble(i, (double) obj);
				} else if (type.equals("BIT")) {
					ps.setBoolean(i, (Boolean) obj);
				} else if (type.equals("DATETIME")) {
					java.sql.Timestamp x = (java.sql.Timestamp) obj;
					ps.setTimestamp(i, x);
				} else {
					ps.setString(i, (String) obj);
				}

			}

			ps.execute();

		} catch (SQLException e) {
			JOptionPane
					.showMessageDialog(null,
							"Something went wrong while ADDING a table. Check data types or field length.");
			Start.error_flag = true;
			e.printStackTrace();
		}
	}

	public void removeRow(String table, int id) {
		try {
			String query ="DELETE ["+table+".*] FROM `"
					+ table + "` WHERE `���`=? ";
			PreparedStatement ps = connection.prepareStatement(query);
			
			ps.setInt(1, id);
			System.out.println(query);
			ps.execute();

		} catch (SQLException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"Something went wrong while REMOVING a table. Check data types or field length.");
			Start.error_flag = true;
			e.printStackTrace();
		}
	}
}
